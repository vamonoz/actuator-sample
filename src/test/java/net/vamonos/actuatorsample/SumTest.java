package net.vamonos.actuatorsample;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SumTest {
    @Test
    public void testSum() {
        Sum sum = new Sum();
        int result = sum.add(2, 2);
        assertEquals(4, result);
    }
}