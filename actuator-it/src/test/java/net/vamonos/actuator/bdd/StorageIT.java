package net.vamonos.actuator.bdd;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:features/storage", plugin = {"pretty", "html:target/cucumber-html"})
public class StorageIT {

}
